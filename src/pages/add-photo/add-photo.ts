import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { storage } from 'firebase';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-add-photo',
  templateUrl: 'add-photo.html',
})
export class AddPhotoPage {
  title:string;
  desc:string;
  photoLink: any;
  phototaken:boolean = false;
  photochosen: boolean = false;
  resultPictureTaken: any = null;
  resultPictureChosen: any = null;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewCtrl: ViewController, private cam: Camera, public zone: NgZone,
              public filechoice :FileChooser, public filepath : FilePath, public file: File) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPhotoPage');
  }

  backToAlbum(){
    let info ={
      Intitule : "",
      Description: "",
      LienPhoto: ""
    };
    this.viewCtrl.dismiss(info);
  }
  async takePhoto(){
    try {
      const options: CameraOptions = {
        quality: 50,
        targetHeight : 600,
        targetWidth : 600,
        destinationType : this.cam.DestinationType.DATA_URL,
        encodingType : this.cam.EncodingType.JPEG,
        mediaType : this.cam.MediaType.PICTURE,
        correctOrientation : true 
      }

      this.resultPictureTaken = await this.cam.getPicture(options);
      this.phototaken = true;
    } catch (error) {
      
    }
      
  }

  fileChoice(){
    /* Gestion du File Chooser */
       this.filechoice.open()
       .then(res =>{ // res == File URI
        this.filepath.resolveNativePath(res).then( file =>{
          this.resultPictureChosen = file;
          this.photochosen = true;
        });  
       }).catch(err =>{
          alert(JSON.stringify(err));
       });
    /* End Gestion File Chooser */ 

  }
  
  validData(){
    console.log("Log check value Title: "+JSON.stringify(this.title));
    console.log("Log check value Desc: "+JSON.stringify(this.desc));
    
    // Si la photo a été prise par la camera de l'appareil
    if(this.phototaken && this.title != "" && this.resultPictureTaken != null){
      const image  = 'data:image/jpeg;base64,'+this.resultPictureTaken+'';
      const pict = storage().ref(this.title);        
      pict.putString(image, 'data_url').then((res) => {
        pict.getDownloadURL().then((url) => {
          this.zone.run(() =>{
            this.photoLink = url;        
          })
          let info ={
            Intitule : this.title,
            Description: this.desc,
            LienPhoto: this.photoLink
          };
          this.viewCtrl.dismiss(info);
        });
      }).catch((err) => {
        alert(JSON.stringify(err));
      });
          
    }else if(this.photochosen && this.title != "" && this.resultPictureChosen != null){
      (<any>window).resolveLocalFileSystemURL(this.resultPictureChosen, (res) => {
        res.file((resFile) => {
        var reader = new FileReader();
        reader.readAsArrayBuffer(resFile);
        reader.onloadend = (evt: any) => {
          var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
          var imageStore = storage().ref(this.title);
          imageStore.put(imgBlob).then((res) => {
            imageStore.getDownloadURL().then((url) => {
              this.zone.run(() =>{
                this.photoLink = url;        
              })
              let info ={
                Intitule : this.title,
                Description: this.desc,
                LienPhoto: this.photoLink
              };
              this.viewCtrl.dismiss(info);
            });
          }).catch((err) => {
            alert(JSON.stringify(err));
          });
        }
        });
      });
    
    }}
}
