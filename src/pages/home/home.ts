import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { GooglePlus } from '@ionic-native/google-plus';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import {Observable} from "rxjs/Observable";
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import {AlbumPage} from '../album/album';
import {InscriptionPage} from '../inscription/inscription';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnDestroy, OnInit {
  private ngUnsubscribe: Subject<boolean> = new Subject();
  user: Observable<firebase.User>;

  constructor(public navCtrl: NavController,public authCtrl:AngularFireAuth,
    public fireDB: AngularFireDatabase,public msg:ToastController, public google:GooglePlus) {
      this.user = this.authCtrl.authState;
  }

  ngOnInit(){ }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
//------------------------- Begin ----------------------

  // Normal Authentification
  async goToAlbum(login:string,mdp:string) {
    try {
      const result  = await this.authCtrl.auth.signInWithEmailAndPassword(login,mdp);  
      if(result){
        this.authCtrl.auth.onAuthStateChanged((user) => {
          if (user) {
            this.navCtrl.push(AlbumPage,{
              user: user
            });
          }
        });
      }
    }catch (error) {
        this.msg.create({
          message: error,
          duration: 5000
        }).present();
    } 
  }
  // Google Authentification (Native)
  loginGoogle(){
    try {
      this.google.login({
        'webClientId' : '767247251591-5hptlkgc3cujc4b7aos12gla7af4jqce.apps.googleusercontent.com',
        'offline' : true,
      }).then(res=>{
        firebase.auth().signInWithCredential(
          firebase.auth.GoogleAuthProvider.credential(res.idToken)
         ).then(success =>{
            alert("Google Login succesful!");
			    this.authCtrl.auth.onAuthStateChanged((user) => {
          if (user) {
            this.fireDB.object('/Users/')
            .snapshotChanges().map(action => {
              const data = action.payload.toJSON();
              return data;
            }).takeUntil(this.ngUnsubscribe)
            .subscribe(result => {
              let returnedArray = [];
              Object.keys(result).map(key => {
                returnedArray.push({ 'key': key, 'data':result[key]});
              }); 
              let affined = returnedArray.map(val => val.key);
              let check  = false;
              while(affined.length > 0){
                let current_id = affined.pop();
                if(current_id === user.uid){
                  check = true;
                }
              }
              console.log("Utilisateur existant ? : "+check); 
              
              if(check){
                this.ngOnDestroy()
               console.log(" Compte deja existant -> Connexion normale");
               this.navCtrl.push(AlbumPage,{
                 arg1: user
               });
              }else{
                console.log(" Nouveau compte -> Inscription + connexion");
                let userbis ={ 
                  firstName : user.displayName,
                  lastName : "Unknown",
                  birthday : "Unknown",
                  sex : "Unknown"             
                };
                this.fireDB.list('/Users').set(user.uid, userbis);
                this.ngOnDestroy()
                this.navCtrl.push(AlbumPage,{
                  arg1: user
                });
              }
            });
          }
        });
         }).catch(fail => {
            alert(JSON.stringify(fail));
         });
      }).catch(err => {
        alert(JSON.stringify(err));
     });
     
    } catch (error) {
      console.log(error);
    }
     
  }

  // Inscription
  public createUser() {
    this.navCtrl.push(InscriptionPage);
  }
}
