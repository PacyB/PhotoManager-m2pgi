import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { storage, database } from 'firebase';

import { Photos } from "../../model/model";
//import { mod} from '../pages/mod-picture/mod-picture';
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@IonicPage()
@Component({
  selector: 'page-album',
  templateUrl: 'album.html',
})
export class AlbumPage implements OnDestroy, OnInit {
  private ngUnsubscribe: Subject<boolean> = new Subject();
  private path: string;
  Pictures : Photos[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public modalCtrl: ModalController, private DataB: AngularFireDatabase,
              private authCtrl: AngularFireAuth, public alertCtrl: AlertController) {
                this.path = 'Users/'+this.navParams.get('user').uid+'/Album/';
                console.log(this.navParams.get('user').uid);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlbumPage');
  }
  ngOnInit(){
      database().ref(this.path).once('value', (snapshot) =>{
          snapshot.forEach( snap => {
            this.Pictures.push(snap.val());
            return false; 
          });     
      });

      //console.log(this.Pictures);
  };
  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  };
  refresh(){
    this.DataB.list(this.path).valueChanges().takeUntil(this.ngUnsubscribe).subscribe(data =>{
      this.Pictures = data as Photos[];
    });
    //console.log(this.Pictures);
  }

  addPhoto(){
    let modal = this.modalCtrl.create('AddPhotoPage');
    modal.onDidDismiss((info) => {
      if(info.Intitule !=""){
        let addedItem = {
          title: info.Intitule,
          desc: info.Description,
          linkPict: info.LienPhoto
        }
        this.DataB.list(this.path).push(addedItem);
        this.refresh();
      }
    })
    modal.present();
  }
  editPhoto(photo : Photos){
    let modal = this.modalCtrl.create('ModPicturePage');
    modal.onDidDismiss((info) => {
      if(info.Intitule !=""){
        let returnedArray = [];
            var modItemIndex = this.Pictures.findIndex(d => d.title == photo.title);
            var UpdatedPhoto = {
              title: info.Intitule,
              desc: info.Description,
              linkPict : photo.linkPict
            }
            this.DataB.object(this.path).snapshotChanges().map(action => {
              const data = action.payload.toJSON();
              return data;
            }).takeUntil(this.ngUnsubscribe)
            .subscribe(result => {
              Object.keys(result).map(key => {
                returnedArray.push({ 'key': key, 'data':result[key]});
              });
              this.DataB.list(this.path).update(returnedArray[modItemIndex].key,UpdatedPhoto);
              this.refresh();
            });
      }
    })
    modal.present();
  }
  sharePhoto(photo : Photos){
    
  }  
  deletePhoto(photo : Photos){
    let confirm = this.alertCtrl.create({
      title: 'Suppression',
      message: 'Etes vous sûr de vouloir supprimer cette photo ?',
      buttons: [
        {
          text: 'Non',
          handler: () => {
            console.log('Non clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Oui clicked');
            console.log("Log check value Item: "+JSON.stringify(photo.title));
            let returnedArray = [];
            var deletedItemIndex = this.Pictures.findIndex(d => d.title == photo.title);
            this.DataB.object(this.path).snapshotChanges().map(action => {
              const data = action.payload.toJSON();
              return data;
            }).takeUntil(this.ngUnsubscribe)
            .subscribe(result => {
              Object.keys(result).map(key => {
                returnedArray.push({ 'key': key, 'data':result[key]});
              });
              this.DataB.list(this.path).remove(returnedArray[deletedItemIndex].key);
              this.refresh();
            });
          }
        }
      ]
    });
    confirm.present();
  }
  async logOut(){
    try {
      await this.authCtrl.auth.signOut().then(rej =>{
          this.DataB.database.ref(this.path).onDisconnect().cancel();        
          this.path = "";
          this.navCtrl.popToRoot();
          console.log('Disconnected successfully!');
      });       
    }catch (error) {
        console.log("Probleme de deconnexion");
    }
  }

}
