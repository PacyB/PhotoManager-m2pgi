import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModPicturePage } from './mod-picture';

@NgModule({
  declarations: [
    ModPicturePage,
  ],
  imports: [
    IonicPageModule.forChild(ModPicturePage),
  ],
})
export class ModPicturePageModule {}
