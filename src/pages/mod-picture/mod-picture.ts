import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-mod-picture',
  templateUrl: 'mod-picture.html',
})
export class ModPicturePage {
  title:string;
  desc:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
      public viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModPicturePage');
  }

  backToAlbum(){
    let info ={
      Intitule : "",
      Description: "",
      LienPhoto: ""
    };
    this.viewCtrl.dismiss(info);
  }

  validData(){
    console.log("Log check value Title: "+JSON.stringify(this.title));
    console.log("Log check value Desc: "+JSON.stringify(this.desc));
    let info ={
      Intitule : this.title,
      Description: this.desc,
      LienPhoto: ""
    };
    this.viewCtrl.dismiss(info);
  }

}
