export interface User {
    email : string,
    password : string,
    firstName : string,
    lastName : string,
    birthday : string,
    sex : string 
  }
  export interface Photos {
    title : string,
    desc : string,
    linkPict : string
  }